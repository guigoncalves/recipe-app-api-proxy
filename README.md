# Recipe App api Proxy

Nginx proxy app for our receipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen (default: `8080`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)